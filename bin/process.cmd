@ECHO OFF

REM Default pipeline running script. Assumes you have Python accessible via 
REM command line. Please check that you can run python by typing python.exe
REM in the command line and press enter. When in python prompt you can press
REM Ctrl-Z and then Enter to quickly exit python. We recommend using 64 bit
REM Anaconda (2.7 or 3.6) python with netcdf and/or pymzml installed for 
REM import of REM NetCDF and mzML formats respectively. 
REM
REM For Anaconda you can run:
REM
REM pip install netcdf 
REM
REM to install netcdf library and run:
REM
REM conda install pymzml
REM
REM to install mzML support library. Other dependencies (bokeh, NumPy, SciPy, 
REM Scikit-learn etc.) are usually automatically included in Anaconda python. 
REM 
REM To run the processing pipeline, type in the command line the following and
REM press Enter:
REM
REM process.cmd "Path_to_Raw_files" "Path_for_Results_Output"
REM
REM Please make sure the source path exists and en-quote it if it has spaces
REM Please make sure the results output path can be created/written to and
REM also placed in quotes if it contains spaces. We also recommend not to use
REM spaces in file and directory names.
REM
REM This script will check first if you have *.cdf files in your folder and
REM will import them. If not found it will try to import *.mzml files.
REM


SETLOCAL ENABLEEXTENSIONS
SET me=%~n0.cmd
SET parent=%~dp0

SET SourcePath=%~1
SET OutputPath=%~2

ECHO %me%: Starting default pipeline processing for GC-MS Data....
ECHO( 

IF "%SourcePath%"=="" (
ECHO Source path not specified... Assuming current folder...
ECHO(
SET SourcePath=%~dp0 
)

SET ResultsSubFolder=Results\

IF "%OutputPath%"=="" (
ECHO Output path not specified... Assuming [current folder]\Results...
ECHO(
SET OutputPath=%~dp0%ResultsSubFolder%
)

ECHO(
ECHO %me%: Source path: %SourcePath%
ECHO %me%: Results path: %OutputPath%
ECHO(
ECHO %me%: Starting import....





 