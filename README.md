﻿﻿MSHub Brief Summary
=============

The __MSHub__ package includes a set of modules for optimised analytical pre-processing workflow for raw chromatography-mass spectromtery data. It accounts for common bio-analytical complexities (including data volume burden, platform-specific biases and heteroscedastic noise structure) inherent to high-throughput GC-MS or LC-MS datasets of biological samples. All pre-processing modules have been designed to operate in an iterative fashion (_i.e._ processing a single dataset at a time) and are thus scalable for processing of hundreds to thousands of samples.

This workflow is an essential prerequisite for processing and integrating heterogeneous chromatography-mass spectromtery datasets for downstream machine learning approaches.

### Description/parameters for each module can be viewed by option “--h or --help”:
python module.py --help

 *  __importmsdata.py__  Imports data from multiple netCDF files to hdf5 database file
 *  __intrapalign.py__ Intra-sample (within sample) m/z peaks drift correction
 *  __noisefilter.py__ Ion Chromatogram noise filtering and baseline correction
 *  __interpalign.py__ Inter-sample (between samples) chromatographic peak alignment
 *  __peakdetect.py__ Efficient detection of chromatographic peaks 
 *  __vistic.py__ Total Ion Chromatogram visualisation


## Setup and Test:
-----

Install [Anaconda Python](https://www.anaconda.com/download).  The platform should work with both Python 3.6 and 2.7, but we recommend to install Python 3.6 64bit for future support and compatibility

Create environment for __MSHub__ within Anaconda Python by running the following commands in the terminal (macOS/Linux) or Anaconda Prompt (Windows): 

#### Windows: ####

```
conda create -n MSHub python=3.6
activate MSHub
conda install -n MSHub bokeh h5py numpy scipy pandas scikit-learn sphinx spyder jupyter statsmodels
conda install -n MSHub -c bioconda pymzml
conda install -n MSHub -c bioconda netcdf4
```

#### macOS/Linux: ####
```
[sudo] conda create -n MSHub python=3.6
[sudo] source activate MSHub
[sudo] conda install -n MSHub bokeh h5py numpy scipy pandas scikit-learn sphinx spyder jupyter statsmodels
[sudo] conda install -n MSHub -c bioconda pymzml
[sudo] conda install -n MSHub -c bioconda netcdf4
```
**Note** optional `sudo` command for Linux/macOS - you may or may not need to use it depending where and how you installed Anaconda. If you can get it to run without `sudo` - this is a preferred option.

Download the current [__MSHub__ release](https://bitbucket.org/iAnalytica/mshub_process/src/master/). Make sure to place it in the folder with no spaces in its path and no special characters. Also avoid using excessively long paths or placing it directly to the root folder on __Windows__.  We recommend using [git](https://git-scm.com/) to do it as this will simplify updates in the future for you:

```

cd <Where you want MSHub>
mkdir MSHub
cd MSHub
git clone https://bitbucket.org/iAnalytica/mshub_process.git .
``` 

If you intend to run test processing using demo data, download it from [MSHub DemoData](https://www.dropbox.com/scl/fo/oygoejqw12uyhb38sq8o8/AADEAlWpfsESLTWcJugfRcs?rlkey=6zvc5pw8st6dh6euvxsg23zk4&st=mopky60s&dl=0) and unpack. Its subfolder __Raw__ will contain raw input data from the calibration dataset. You can test the processing workflow using this data before applying it to your own one. The demo folder will also contain the output from the previous processing of the test data for your reference (_i.e._ "ground truth"). 

Inside your activated MSHub environment console navigate to the folder with MSHub code and run jupyter notebook. 

__Windows__ example:
```
(MSHub) C:\Users\Ivan>D:
(MSHub) C:\Users\Ivan>cd D:\Work\Imperial\MSHub
(MSHub) D:\Work\Imperial\MSHub\>jupyter-notebook
```
__macOS/Linux__ example:
```
(MSHub) /home/Ivan>
(MSHub) /home/Ivan>cd /home/Ivan/MSHub
(MSHub) /home/Ivan/MSHub>jupyter-notebook
```

__Jupyter__ notebook should open in your browser. When it does - navigate into __jupyernotebooks__ subfolder in your __MSHub__ copy and open __walkthrough.ipynb__ notebook with the example walkthough. Once open - navigate its instructions to process data. Use the demo data provided if you want to test the workflow first. Modify and adapt __walkthrough__ workflow for your data as needed by __"Save as..."__ and adjusting. 

Whenever you need to process your new batch of data - just open a terminal/Anaconda prompt, navigate to the __MSHub__ code and start __Jupyter__ notebook again. From there you can open the __walkthrough__ workflow, edit it and run to do the processing. We recommend creating copies of the original notebook before editing, so you can always return to its original in case your modifications broke anything. __Note__ that the folder you run __jupyter-notebook__ in is the folder which becomes its __root__ folder - thus the need to navigate to the __MSHub__ source code folder. You will not be able to go above the __root__ folder in your __Jupyter__ notebook file browser.
 

Authors
-------
Lead Developers: 
[Dr. Ivan Laponogov](mailto:i.laponogov@imperial.ac.uk), 
[Dr. Dennis Veselkov](mailto:d.veselkov@imperial.ac.uk)
Chief project investigator: 
[Dr. Kirill Veselkov](mailto:kirill.veselkov04@imperial.ac.uk)


Acknowledgments
---------------
Any papers describing data analytics using this package, or whose results were significantly 
aided by the use of this package (except when the use was internal to a larger program), 
should include an acknowledgment and citation to the following manuscript(s): 

1. Alexander A. Aksenov _et al_. Algorithmic Learning for Auto-deconvolution of GC-MS Data to Enable Molecular Networking within __GNPS__. (2020) _Nature Biotechnology_ (_submitted_).


If the package is used prior to its open source release and publication, please contact [kirill.veselkov04@imperial.ac.uk](mailto:kirill.veselkov04@imperial.ac.uk) to ensure that all contributors are fairly acknowledged.  

Issues
------

Please report any bugs or requests that you have using the Bitbucket issue tracker!

Licenses
-----------

The code which makes up this Python project template is licensed under the permissive BSD type license (see License.txt). 
Feel free to use it in your free software/open-source or proprietary projects.

The template also uses a number of other pieces of software, whose licenses are listed here for convenience. It is your responsibility to ensure that these licenses are up-to-date for the version of each tool you are using.

| Project | License |
|------|------|
| Anaconda Python  | [3-clause BSD License](https://docs.anaconda.com/anaconda/eula/) |
| Sphinx |  Simplified BSD License |
| NumPy  |  BSD License |
| SciPy  |  BSD License |
| H5Py | BSD License |
| scikit-learn | BSD License |
| Bokeh | BSD License |
| netcdf4 | [OSI Approved and MIT](http://opensource.org/search/node/OSI+Approved+and+MIT) |
| pymzml | [GNU GPL](http://opensource.org/search/node/GNU+General+Public+License+%28GPL%29) |


