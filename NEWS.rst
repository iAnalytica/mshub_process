News
====

10.01.2017
-----

*BASIS 1.0 release*

Brief Summary
=============

The package includes a set of modules for optimized analytical 
pre-processing workflow for raw MSI data. It accounts for common 
bio-analytical complexities (including data volume burden, 
platform-specific biases and heteroscedastic noise structure) 
inherent to high-throughput MALDI- and DESI-MSI datasets of 
tissue specimens. All pre-processing modules have been designed to 
operate in an iterative fashion (i.e. processing a single dataset at a time) 
and are thus scalable for processing of hundreds to thousands of specimens.

This workflow is an essential prerequisite 
for processing and integrating heterogeneous 
MSI datasets for downstream machine learning approaches.

The modules include:  

    *  **HDF5-based Database Management** to organise, retrieve and store large volumes of numerical MS imaging datasets

    *  **Peak Alignment** to account for non-linear drifts of mass to charge or collision cross section measurements of a given molecule between multiple datasets                   
        
    *  **Inter-sample normalization** to adjust for spectrum-to-spectrum differences in overall scale within individual datasets.  
    
    *  **Intra-sample normalization** to bring MSI datasets of multiple samples into a common scale 

    *  **Variance-stabilizing transformation** to account for increased technical variability as a function of increased intensity

    *  **Cluster-driven peak filtering** to remove solvent or matrix related molecular ion features