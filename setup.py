# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import imp

from setuptools import setup, find_packages

if sys.version_info < (2, 6, 0, 'final', 0):
    raise SystemExit('Python 2.6 or later is required!')
    
def read(*path):
        return open(os.path.join(os.path.abspath(os.path.dirname(__file__)), *path)).read()

VERSION = '0.1'
README = read('README.md')
NEWS = read('NEWS.rst')
# Not version specific dependencies
# install_requires = ['scipy', 'numpy', 'h5py', 'time']

# Add the current directory to the module search path.
sys.path.insert(0, os.path.abspath('.'))

## Constants
CODE_DIRECTORY = 'proc'
DOCS_DIRECTORY = 'docs'
TESTS_DIRECTORY = 'tests'
PYTEST_FLAGS = ['--doctest-modules']

README = read('README.md')
NEWS = read('NEWS.rst')


metadata = imp.load_source(
    'pckginfo', os.path.join(CODE_DIRECTORY, 'pckginfo.py'))


# define install_requires for specific Python versions
python_version_specific_requires = []

# as of Python >= 2.7 and >= 3.2, the argparse module is maintained within
# the Python standard library, otherwise we install it as a separate package
if sys.version_info < (2, 7) or (3, 0) <= sys.version_info < (3, 3):
    python_version_specific_requires.append('argparse')


# See here for more options:
# <http://pythonhosted.org/setuptools/setuptools.html>
config = dict(
    name=metadata.package,
    version=metadata.version,
    author=metadata.authors[0],
    author_email=metadata.emails[0],
    maintainer=metadata.authors[0],
    maintainer_email=metadata.emails[0],
    url=metadata.url,
    description=metadata.description,
    long_description = README + '\n\n' + NEWS,
    # Find a list of classifiers here:
    # <http://pypi.python.org/pypi?%3Aaction=list_classifiers>
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Topic :: Scientific/Engineering :: Bio-Informatics'
    ],
    packages=find_packages(exclude=(TESTS_DIRECTORY,)),
    install_requires=['netCDF4'
        # your module dependencies
    ] + python_version_specific_requires,
    include_package_data=True,
    zip_safe=False,  # don't use eggs
  
)


def main():
    setup(**config)


if __name__ == '__main__':
    main()
