# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 17:56:47 2017

@author: ilaponog
"""
import numpy as np;
import os;

from bokeh.plotting import figure, ColumnDataSource;
from bokeh.resources import CDN
from bokeh.embed import components
from bokeh.layouts import gridplot #, column
from bokeh.palettes import Inferno256;
from bokeh.models import ColorBar, LinearColorMapper, BasicTicker, HoverTool;
from bokeh.io import save;


def load_integral_table(fname):
    dataset_names = [];
    data_values = [];
    rts = [];
    mzs = [];
    with open(fname, 'r') as finp:
        index = -1;
        for s in finp:
            index += 1;
            print(index)
            s = s.rstrip().split(',');
            if index == 0:
                dataset_names = s[5:-1];
                for i in range(len(dataset_names)):
                    print(dataset_names[i])
                    dataset_names[i] = dataset_names[i][0:dataset_names[i].index('.mzML')];
            else:
                rts.append(float(s[3]) * 60.0);
                mzs.append(float(s[2]));
                dv = s[5:-1];
                for i in range(len(dv)):
                    dv[i] = float(dv[i]);
                data_values.append(np.array(dv, dtype = np.float64));
                
    return dataset_names, np.vstack(data_values), np.array(rts), np.array(mzs)
                
def load_our_table(fname):
    dataset_names = [];
    data_values = [];
    rts = [];
    mzs = [];
    
    d = [];
    rt = [];
    mz = [];
    integ = [];    
    rtmz = [];
    
    with open(fname, 'r') as finp:
        index = -1;
        for s in finp:
            index += 1;
            if index % 1000 == 0:
                print(index)
            if index > 0:
                s = s.rstrip().split(',');
                d.append(s[0].lstrip('"').rstrip('"'));
                rtmz.append(s[1] + ',' + s[2]);
                
                integ.append(float(s[4]));
                
    dataset_unique = sorted(list(set(d)));
    
    dataset_index = {};
    for i in range(len(dataset_unique)):
        dataset_index[dataset_unique[i]] = i;
                
    rtmz_unique = sorted(list(set(rtmz)));
    
    rtmz_index = {};    
    
    for i in range(len(rtmz_unique)):
        s = rtmz_unique[i].split(',');
        rt.append(float(s[1]))
        mz.append(float(s[0]))
        rtmz_index[rtmz_unique[i]] = i;
    print(len(rtmz_unique))        
    print(len(dataset_unique))        
    print(len(rtmz_unique)*len(dataset_unique))        
    total_data = np.zeros((len(rtmz_unique), len(dataset_unique)), dtype = np.float64);
    
    for i in range(len(integ)):
        if i%100==0:
            print(i, len(integ))
        total_data[rtmz_index[rtmz[i]], dataset_index[d[i]]] = integ[i];
    


    return dataset_unique, total_data, np.array(rt), np.array(mz)    



def match_lists(list1, list2, return_inverse = False):
    """
    Matching of two lists.
    
    Args:
        list1, list2  - two lists to be matched
        return_inverse - return inverse conversion indeces or not, default = False
        
    Returns:
        indeces1 - array of conversion indeces (dtype = np.int64) 
                      from list1 to list2, i.e. for i in range(len(list1))
                      list1[i] is matched to list2[indeces1[i]],
                      indeces1 has same size as list1
                      "-1" numerical value in indeces1[i] indicates no match
        
        indeces2 - (optional) array of inverse conversion indeces 
                      (dtype = np.int64) i.e. for i in range(len(list2))
                      list2[i] is matched to list1[indeces2[i]],
                      indeces2 has same size as list2
                      "-1" numerical value in indeces2[i] indicates no match
    
    """
    indeces1 = [];
    for s in list1:
        try:
            #for each element in list1 try to find corresponding index 
            #in list2
            indeces1.append(list2.index(s));
        except:
            #if none found - set index to -1
            indeces1.append(-1);
    
    #convert to numpy array
    indeces1 = np.array(indeces1, dtype = np.int64);

    if return_inverse:
        #prepare output array indeces2 pre-filled with -1 for no matches
        indeces2 = np.full((len(list2), ), -1, dtype = np.int64);
        #get mask of matched indeces
        list1_mask = indeces1 >= 0;
        #get indeces of matches
        list1_indcs = np.arange(len(list1), dtype = np.int64);
        #assign them to inverse indeces
        indeces2[indeces1[list1_mask]] = list1_indcs[list1_mask];
        return indeces1, indeces2;
    else:
        return indeces1


def toarray(data, rt, mz):
    rt_unique = np.unique(rt);
    #print(rt_unique.shape)
    
    mz_unique = np.unique(mz);
    #print(mz_unique.shape)
    out_data = np.zeros((len(rt_unique), len(mz_unique)), dtype = np.float64);
    rt_index = {};
    mz_index = {};
    for i in range(len(rt_unique)):
        rt_index[rt_unique[i]] = i;
    for i in range(len(mz_unique)):
        mz_index[mz_unique[i]] = i;

    for i in range(len(data)):
        rti = rt_index[rt[i]];
        mzi = mz_index[mz[i]];
        out_data[rti, mzi] = data[i];
    
    return out_data, rt_unique, mz_unique
    

def nn_match(rt, crt, tolerance = 0.1001, return_inverse = False):
    """
    Recursive matching of two 1D NumPy arrays of values with given tolerance.
    
    Args:
        rt, crt - two 1D NumPy arrays to be matched
        tolerance - allowed matching distance between values, default = 0.1001
        return_inverse - return inverse conversion indeces or not, default = False
        
    Returns:
        rt2crtindcs - array of conversion indeces (dtype = np.int64) 
                      from rt to crt, i.e. for i in range(rt.shape[0])
                      rt[i] is matched to crt[rt2crtindcs[i]],
                      rt2crtindcs has same size as rt
                      "-1" numerical value in rt2crtindcs[i] indicates no match
                      with given tolerance
        
        crt2rtindcs - (optional) array of inverse conversion indeces 
                      (dtype = np.int64) i.e. for i in range(crt.shape[0])
                      crt[i] is matched to rt[crt2rtindcs[i]],
                      crt2rtindcs has same size as crt
                      "-1" numerical value in crt2rtindcs[i] indicates no match
                      with given tolerance
    
    """
    
    if len(rt.shape) != 1:
        raise TypeError('rt should be 1D NumPy array!');

    if len(crt.shape) != 1:
        raise TypeError('crt should be 1D NumPy array!');
        
    #indeces for array subselections    
    crt_ind_range = np.arange(crt.shape[0], dtype = np.int64);
    rt_ind_range = np.arange(rt.shape[0], dtype = np.int64);

    # efficient nearest-neighbour alignment of retention time vector to the common feature vector
    rt2crtindcs = np.round(np.interp(rt, crt, np.arange(0., len(crt))))
    rt2crtindcs = (rt2crtindcs.astype(int)).flatten()
                
    # remove all matched pairs smaller than pre-defined or calculated torelance 
    # -1 indicates no match
    rt2crtindcs[np.abs(crt[rt2crtindcs] - rt) > tolerance] = -1;
    
    # remove repetitions leaving the closest match only
    #get unique link indeces and their counts
    u, uc = np.unique(rt2crtindcs, return_counts = True);
    #get indeces with counts more than 1
    mids = u[uc > 1];
    #exclude mismatched indeces marked as -1
    mids = mids[mids >=0 ];
    
    #prepare the list for indeces to be re-processed recursively
    redo_list = [];
    
    #Iterate through indeces with multiple matches (i.e. non-unique ones)    
    for i in range(mids.shape[0]):
        #get rt indeces of the non-unique match
        rti = rt_ind_range[rt2crtindcs == mids[i]];
        #calculate distances for non-unique matches
        dist = np.abs(crt[mids[i]] - rt[rti]);
        #get position of the best match
        best = np.argmin(dist);
        #set all indeces to -2 to indicate the non-unique match
        rt2crtindcs[rti] = -2;
        #but set the best match to one closest in dist
        rt2crtindcs[rti[best]] = mids[i];
        #add the remaining indeces to the list of the ones needing re-iteration
        redo_list.append(np.delete(rti, best));

    #check if re-iteration is needed (i.e. there are non-matched non-unique entries left)    
    if redo_list:
        #get full list of indeces for re-matching
        redo_rt_idcs = np.hstack(redo_list);
        #prepare boolean mask for unmatched indeces
        unmatched_crts = np.full(crt.shape, True, dtype = np.bool);
        #get the list of uniquely matched indeces
        matched_crts = rt2crtindcs[rt2crtindcs >= 0];
        #exclude entries which were uniquely matched from the mask
        unmatched_crts[matched_crts] = False;
        #get the list of unmatched indeces
        redo_crt_idcs = crt_ind_range[unmatched_crts];
        #call matching recursively for unmatched and non-uniquely matched indeces
        sub_rt2crt = nn_match(rt[redo_rt_idcs], crt[redo_crt_idcs], tolerance);
        #get mask for newly matched indeces
        sub_rt2crt_unmatched = sub_rt2crt < 0;
        #get inverse mask for completely unmatched indeces
        sub_rt2crt_matched = np.logical_not(sub_rt2crt_unmatched);
        #set completely unmatched indeces to -1
        rt2crtindcs[redo_rt_idcs[sub_rt2crt_unmatched]] = -1;
        #set re-matched indeces to the newly detected values from recursive call
        rt2crtindcs[redo_rt_idcs[sub_rt2crt_matched]] = redo_crt_idcs[sub_rt2crt[sub_rt2crt_matched]];
        
    if return_inverse:
        #create and pre-fill crt2rtindcs with -1 to cover non-matched indeces
        crt2rtindcs = np.full(crt.shape, -1, dtype = np.int64);
        #get mask of matched rt indeces
        match_rt2crt_mask = rt2crtindcs >=0;
        #get array of matched crt indeces
        matched_crts = rt2crtindcs[match_rt2crt_mask];
        #get array of corresponding rt indeces
        matched_rts = rt_ind_range[match_rt2crt_mask];
        #assign matched inverse indeces
        crt2rtindcs[matched_crts] = matched_rts;
        return rt2crtindcs, crt2rtindcs;
    else:
        return rt2crtindcs;


def correlate(data_1, data_2, rt_1, mz_1, rt_2, mz_2):
    data_1, rt_1, mz_1 = toarray(data_1, rt_1, mz_1);
    data_2, rt_2, mz_2 = toarray(data_2, rt_2, mz_2);
    
    rt_match = nn_match(rt_1, rt_2, tolerance = 27.0);
    
    mz_match = nn_match(mz_1, mz_2, tolerance = 1.5);
    
    rt_matched = rt_match >= 0.0;
    
    mz_matched = mz_match >= 0.0;
    
    #print(data_1.shape, rt_matched.shape, mz_matched.shape)
    subdata1 = data_1[rt_matched, :];
    
    subdata1 = subdata1[:,  mz_matched];
    
    subdata2 = data_2[rt_match[rt_matched], :];
    subdata2 = subdata2[:, mz_match[mz_matched]];
    
    int1 = np.sum(subdata1)/np.sum(data_1);
    int2 = np.sum(subdata2)/np.sum(data_2);
    
    
    subdata1 = subdata1.flatten()    
    subdata2 = subdata2.flatten()    
    
    corr = np.corrcoef(subdata1, subdata2)
    
    return corr, int1, int2


def correlate1d(data_1, data_2, rt_1, mz_1, rt_2, mz_2):
    data_1, rt_1, mz_1 = toarray(data_1, rt_1, mz_1);
    data_2, rt_2, mz_2 = toarray(data_2, rt_2, mz_2);
    
    rt_match = nn_match(rt_1, rt_2, tolerance = 27.0);
    
    mz_match = nn_match(mz_1, mz_2, tolerance = 1.5);
    
    rt_matched = rt_match >= 0.0;
    
    mz_matched = mz_match >= 0.0;
    
    #print(data_1.shape, rt_matched.shape, mz_matched.shape)
    subdata1 = data_1[rt_matched, :];
    
    subdata1 = subdata1[:,  mz_matched];
    
    subdata2 = data_2[rt_match[rt_matched], :];
    subdata2 = subdata2[:, mz_match[mz_matched]];
    
    int1 = np.sum(subdata1)/np.sum(data_1);
    int2 = np.sum(subdata2)/np.sum(data_2);
    
    
    subdata1 = subdata1.flatten()    
    subdata2 = subdata2.flatten()    
    
    corr = np.corrcoef(subdata1, subdata2)
    
    return corr, int1, int2


median_palette_positive = [];
median_palette_negative = [];
median_palette = [];

for i in range(256):
    median_palette_negative.append(('#%02x%02x%02x'%(0, i, i)).upper());
    median_palette_positive.append(('#%02x%02x%02x'%(i, 0, 0)).upper());
    vv = (float(i) - 255/2.0)*2.0;
    v = int(abs(vv));
    if vv >= 0:
        median_palette.append(('#%02x%02x%02x'%(v, 0, 0)).upper());
    else:
        median_palette.append(('#%02x%02x%02x'%(0, v, v)).upper());


def plotit(fname, data1, rt1, mz1, data2, rt2, mz2):
    print(fname)
    data1 = np.log(data1 + 1.0);
    data2 = np.log(data2 + 1.0);
    
    minrt = min(np.min(rt1), np.min(rt2));
    maxrt = max(np.max(rt1), np.max(rt2));
    
    minmz = min(np.min(mz1), np.min(mz2));
    maxmz = max(np.max(mz1), np.max(mz2));

    color = [];    
    
    md1 = np.max(data1)
    md2 = np.max(data2)
    md = max(md1, md2)

    data1 = data1/md*255;
    data1 = data1.astype(dtype = np.int32)
    
    data2 = data2/md*255;
    data2 = data2.astype(dtype = np.int32)
    
    rt = np.hstack([rt1, rt2]);
    mz = np.hstack([mz1, mz2]);
    
    for j in range(len(data1)):
        color.append(median_palette_positive[data1[j]]);

    for j in range(len(data2)):
        color.append(median_palette_negative[data2[j]]);
    
    source = ColumnDataSource(data=dict(
       top = np.subtract(mz, 0.3).tolist(),
       bottom = np.add(mz, 0.3).tolist(),
       left = np.subtract(rt, 3).tolist(),
       right = np.add(rt, 3).tolist(),
       rt = rt.tolist(),
       mz = mz.tolist(),
       color = color,
    ))
    
       
    hover = HoverTool(tooltips=[
        ("rt (sec)", "@rt"),
        ("mz (Da)", "@mz"),
    ])
    
    ticker = BasicTicker();

    s = figure(plot_width = 1200, 
               plot_height = 1000, 
               title = 'rtmz', 
               x_range = (minrt, maxrt),
               y_range = (minmz, maxmz),
               tools = "pan,wheel_zoom,box_zoom,undo,redo,reset", 
               toolbar_location = "above",
               toolbar_sticky = False,
               y_axis_location = "right",
               background_fill_color = "black");
    
    s.xgrid.grid_line_color = None;
    s.ygrid.grid_line_color = None;
    s.add_tools(hover);
    
    s.quad(top = 'top', bottom = 'bottom', left = 'left',
       right = 'right', color = 'color', source = source);        
    
    '''color_bar = ColorBar(color_mapper = color_mapper, 
                         ticker = ticker, 
                         label_standoff = 12, 
                         border_line_color = None, 
                         location = (0, 0),
                         orientation = "horizontal")        

    s.add_layout(color_bar, 'below');
    '''
    save(s, filename = fname);
    

def plotit1(fname, data1, rt1, mz1):
    print(fname)
    data1 = np.log(data1 + 1.0);
    
    minrt = np.min(rt1);
    maxrt = np.max(rt1);
    
    minmz = np.min(mz1);
    maxmz = np.max(mz1);

    color = [];    
    
    md = np.max(data1)
    
    data1 = data1/md*255;
    data1 = data1.astype(dtype = np.int32)
    
    
    rt = rt1
    mz = mz1
    
    for j in range(len(data1)):
        color.append(median_palette_positive[data1[j]]);

    
    
    source = ColumnDataSource(data=dict(
       top = np.subtract(mz, 0.3).tolist(),
       bottom = np.add(mz, 0.3).tolist(),
       left = np.subtract(rt, 3).tolist(),
       right = np.add(rt, 3).tolist(),
       rt = rt.tolist(),
       mz = mz.tolist(),
       color = color,
    ))
    
       
    hover = HoverTool(tooltips=[
        ("rt (sec)", "@rt"),
        ("mz (Da)", "@mz"),
    ])
    
    ticker = BasicTicker();

    s = figure(plot_width = 1200, 
               plot_height = 1000, 
               title = 'rtmz', 
               x_range = (minrt, maxrt),
               y_range = (minmz, maxmz),
               tools = "pan,wheel_zoom,box_zoom,undo,redo,reset", 
               toolbar_location = "above",
               toolbar_sticky = False,
               y_axis_location = "right",
               background_fill_color = "black");
    
    s.xgrid.grid_line_color = None;
    s.ygrid.grid_line_color = None;
    s.add_tools(hover);
    
    s.quad(top = 'top', bottom = 'bottom', left = 'left',
       right = 'right', color = 'color', source = source);        
    
    '''color_bar = ColorBar(color_mapper = color_mapper, 
                         ticker = ticker, 
                         label_standoff = 12, 
                         border_line_color = None, 
                         location = (0, 0),
                         orientation = "horizontal")        

    s.add_layout(color_bar, 'below');
    '''
    save(s, filename = fname);
    
    


fin1 = 'e:/FTP/GCMS/MSV000080892/meta/Table_stilton_bonde_1706.csv';
fin2 = 'i:/FTP/GCMS/MSV000080892/Results5/GC_data_for_Jamie_all_peaks.csv';
fouthtml = 'i:/FTP/GCMS/MSV000080892/Results5/comparison.html'
foutname = 'i:/FTP/GCMS/MSV000080892/Results5/comparison.csv'
finh5 = 'i:/FTP/GCMS/MSV000080892/Results5/GC_data_for_Jamie.h5';

dataset_1, integral_1, rt_1, mz_1 = load_integral_table(fin1);

dataset_2, integral_2, rt_2, mz_2 = load_our_table(fin2)




with open(foutname, 'w') as fout:
    fout.write('Data, N datasets, N measurements(rt+mz))\n');
    fout.write('Their,%s, %s \n'%(len(dataset_1), rt_1.shape[0]))
    fout.write('Our,%s, %s \n'%(len(dataset_2), rt_2.shape[0]))
    
    print(len(dataset_1), integral_1.shape, rt_1.shape, mz_1.shape)
    print(len(dataset_2), integral_2.shape, rt_2.shape, mz_2.shape)
    
    fout.write('Data, Min RT, Max RT, Mean RT, Min MZ, Max MZ, Mean MZ\n');
    fout.write('Their,%s, %s, %s, %s, %s, %s \n'%(np.min(rt_1), np.max(rt_1), np.mean(rt_1), np.min(mz_1), np.max(mz_1), np.mean(mz_1)))
    fout.write('Our,%s, %s, %s, %s, %s, %s \n'%(np.min(rt_2), np.max(rt_2), np.mean(rt_2), np.min(mz_2), np.max(mz_2), np.mean(mz_2)))
    
    print(np.min(rt_1), np.max(rt_1), np.mean(rt_1), np.min(mz_1), np.max(mz_1), np.mean(mz_1));
    print(np.min(rt_2), np.max(rt_2), np.mean(rt_2), np.min(mz_2), np.max(mz_2), np.mean(mz_2));

    
    #matched_datasets = match_lists(dataset_1, dataset_2);
    
    #print(matched_datasets)

    d1to2, d2to1 = match_lists(dataset_2, dataset_1, return_inverse = True);    
    
    #print(our_to_their)
    #print(their_to_our)
    #print(len(our_to_their[our_to_their >= 0]))
    #print(len(their_to_our[their_to_our >= 0]))
    
    dour_to_their = d1to2[d1to2 >= 0];
    dtheir_to_our = d2to1[d2to1 >= 0];
    
    print('Their datasets: Count %s, Overlap: %s, Percentage in overlap: %3.1f%%'%(
        len(dataset_1), len(dtheir_to_our), float(len(dtheir_to_our))/len(dataset_1)*100.0));
    print('Our datasets: Count %s, Overlap: %s, Percentage in overlap: %3.1f%%'%(
        len(dataset_2), len(dour_to_their), float(len(dour_to_their))/len(dataset_2)*100.0));

    matched_data_1 = integral_1[:, d2to1>=0 ];
    matched_data_2 = integral_2[:, d2to1[d2to1 >=0]];
        
    v1 = np.arange(len(d2to1))[d2to1>=0]
    #print(v1.shape)
    v2 = d2to1[d2to1>=0];
    #print(v2.shape)
    
    
    print(np.unique(rt_1).shape, np.unique(rt_2).shape)
    print(np.unique(mz_1).shape, np.unique(mz_2).shape)
    
    print(np.min(np.diff(np.unique(rt_1))));
    print(np.median(np.diff(np.unique(rt_1))));
    print(np.mean(np.diff(np.unique(rt_1))));
    
    print(np.min(np.diff(np.unique(rt_2))));
    
    rt_1 = np.round(rt_1/2)*2;
    
    print(np.unique(rt_1).shape, np.unique(rt_2).shape)


    print(np.min(np.diff(np.unique(mz_1))));
    
    print(np.min(np.diff(np.unique(mz_2))));
    
    mz_1 = np.round(mz_1/2)*2;
    
    print(np.unique(mz_1).shape, np.unique(mz_2).shape)



    corr = [];
    pint_1 = [];
    pint_2 = [];
    
    '''
    with open(fouthtml, 'w') as outhtml:
        samples = [];
        ii1 = np.arange(len(d2to1), dtype = np.int32)[d2to1>=0];
        ii2 = d2to1[d2to1 >=0];
    
        for i in range(matched_data_1.shape[1]):
            print(i)
            c, p1, p2 = correlate(matched_data_1[:,i], matched_data_2[:,i], rt_1, mz_1, rt_2, mz_2);
            c, p1, p2 = correlate1D(matched_data_1[:,i], matched_data_2[:,i], rt_1, mz_1, rt_2, mz_2);
            plotit(os.path.split(fouthtml)[0] + '/%s.html'%dataset_1[ii1[i]], matched_data_1[:,i], rt_1, mz_1, matched_data_2[:,i], rt_2, mz_2);
            plotit1(os.path.split(fouthtml)[0] + '/%s_their.html'%dataset_1[ii1[i]], matched_data_1[:,i], rt_1, mz_1);
            plotit1(os.path.split(fouthtml)[0] + '/%s_our.html'%dataset_2[ii2[i]], matched_data_2[:,i], rt_2, mz_2);
            corr.append(c)
            pint_1.append(p1)
            pint_2.append(p2)
            
        corr = np.array(corr);
        pint_1 = np.array(pint_1);
        pint_2 = np.array(pint_2);
        fout.write('Mean correlation, correlation std, explained intensity theirs mean, explained intensity theirs std, explained intensity our mean,explained intensity our std\n');
    
        fout.write('%s,%s,%s,%s,%s,%s\n'%(np.mean(corr), np.std(corr), np.mean(pint_1), np.std(pint_1), np.mean(pint_2), np.std(pint_2)))
        
    '''
        
    
    
    
    
    