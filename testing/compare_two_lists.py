# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 16:02:24 2017

@author: ilaponog
"""

import os; 
import sys;
import traceback #This is for displaying traceback in try: except: constructs
import time;

#<these will likely be needed for hdf5 support and mathematical operations>
import h5py
import numpy as np

#<Add mode global/external modules here as needed>

#If run independently - check system endianness and add path to local modules
if __name__ == "__main__": 
    if sys.byteorder!='little':
        #Use print here instead of printlog as printlog is not yet imported! 
        #The rest should have printlog in place of print.
        print('Only little endian machines currently supported! bye bye ....');
        quit();
    #<Adjust relative path in the following line to point to the folder containing proc>
    #<Currently proc is located two folders above the one with the template.py>
    module_path = os.path.abspath('%s/../..'%os.path.dirname(os.path.realpath(__file__)));
    #this should place the path for searching local modules at the front 
    #so that local path is found first and your script is not confused by possible
    #other versions in the search path
    sys.path.insert(0, module_path); 
    

#Import local/internal modules

#Import command line options set for <your> module from procconfig.
#<replace "Template_option" with the option set name you created for your module.>
#<See procconfig for template option set.>
from proc.procconfig import Template_options as cmdline_options;

#Manager for command line options
from proc.utils.cmdline import OptionsHolder

#Timing functions for standard stats output
from proc.utils.timing import tic, toc;
from proc.utils.matching import nn_match;

if __name__ == "__main__": 
    
    inf1 = 'g:/FTP/Incoming2/Cheese/our.txt';
    inf2 = 'g:/FTP/Incoming2/Cheese/their.txt';
    
    outpath = 'g:/FTP/Incoming2/Cheese/';
    
    our = [];
    their = [];
    
    with open(inf1, 'r') as finp:
        for s in finp:
            s = s.rstrip('\n').split('\t');
            if len(s)>0:
                our.append([float(s[0]),float(s[1]), float(s[2])]);

    with open(inf2, 'r') as finp:
        for s in finp:
            s = s.rstrip('\n').split('\t');
            if len(s)>0:
                their.append([float(s[0]),float(s[1]), float(s[2])]);
                
                
    our = np.array(our);
    their = np.array(their);

    sindex = np.argsort(our[:, 0]);
    our = our[sindex, :];
    
    sindex = np.argsort(their[:, 0]);
    their = their[sindex, :];
    
    print(our.shape)
    print(their.shape)
    
    our_to_their, their_to_our = nn_match(our[:, 0], their[:, 0], tolerance = 0.15, return_inverse = True);
    
    print(our_to_their.shape);
    
    print(np.sum(our_to_their == -1))

    print(np.sum(their_to_our == -1))
    
    our_mismatched = our[our_to_their == -1, :];
    
    their_mismatched = their[their_to_our == -1, :];
    
    our_matched = our[our_to_their != -1, :];
    their_matched = their[our_to_their[our_to_their != -1], :];
    
    with open(outpath + 'our_unmatched.csv', 'w') as fout:
        fout.write('N, RT(min), qvalue, LogDelta\n');
        for i in range(our_mismatched.shape[0]):
            fout.write('%s, %.2f, %.4f, %.3f\n'%(i+1, our_mismatched[i, 0], our_mismatched[i, 1], our_mismatched[i, 2]));

    with open(outpath + 'their_unmatched.csv', 'w') as fout:
        fout.write('N, RT(min), qvalue, LogDelta\n');
        for i in range(their_mismatched.shape[0]):
            fout.write('%s, %.2f, %.4f, %.3f\n'%(i+1, their_mismatched[i, 0], their_mismatched[i, 1], their_mismatched[i, 2]));
        
    with open(outpath + 'ourandtheir_matched.csv', 'w') as fout:
        fout.write('N, RT(their), qvalue(their), LogDelta(their), Direction, RT(our), qvalue(our), LogDelta(our)\n');
        for i in range(our_matched.shape[0]):
            fout.write('%s, %.2f, %.4f, %.3f'%(i+1, their_matched[i, 0], their_matched[i, 1], their_matched[i, 2]));
            if their_matched[i, 2] * our_matched[i, 2] >= 0.0:
                fout.write(', Same');
            else:
                fout.write(', Anti');
            fout.write(', %.2f, %.4f, %.3f\n'%(our_matched[i, 0], our_matched[i, 1], our_matched[i, 2]));
        
    
    




    
    

    """

def nn_match(rt, crt, tolerance = 0.1001, return_inverse = False):
    Recursive matching of two 1D NumPy arrays of values with given tolerance.
    
    Args:
        rt, crt - two 1D NumPy arrays to be matched
        tolerance - allowed matching distance between values, default = 0.1001
        return_inverse - return inverse conversion indeces or not, default = False
        
    Returns:
        rt2crtindcs - array of conversion indeces (dtype = np.int64) 
                      from rt to crt, i.e. for i in range(rt.shape[0])
                      rt[i] is matched to crt[rt2crtindcs[i]],
                      rt2crtindcs has same size as rt
                      "-1" numerical value in rt2crtindcs[i] indicates no match
                      with given tolerance
        
    """
