basis package
=============

Subpackages
-----------

.. toctree::

    basis.io
    basis.preproc
    basis.utils

Submodules
----------

basis.procconfig module
-----------------------

.. automodule:: basis.procconfig
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: basis
    :members:
    :undoc-members:
    :show-inheritance:
