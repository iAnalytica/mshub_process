basis.io package
================

Submodules
----------

basis.io.importmsi module
-------------------------

.. automodule:: basis.io.importmsi
    :members:
    :undoc-members:
    :show-inheritance:

basis.io.manageh5db module
--------------------------

.. automodule:: basis.io.manageh5db
    :members:
    :undoc-members:
    :show-inheritance:

basis.io.mapfiles module
------------------------

.. automodule:: basis.io.mapfiles
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: basis.io
    :members:
    :undoc-members:
    :show-inheritance:
